BINFOLDER := bin/

INCFOLDER := inc/

SRCFOLDER := src/

OBJFOLDER := obj/

CC := g++

CFLAGS := -Wall -ansi

SRCFILES := $(wildcard src/*.cpp)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) obj/*.o -o bin/finalBinary

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc

run: bin/finalBinary
	bin/finalBinary

.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*
	rm -rf */*~ *~
