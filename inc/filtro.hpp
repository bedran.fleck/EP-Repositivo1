#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <iostream>

using namespace std;

class Filtro{
	private:
		int div;
		int size;
		int *matriz;

	public:
		Filtro();
		Filtro(int div, int size);

		int getDiv();
		int getSize();
		int * getMatriz();

		void setDiv(int div);
		void setSize(int size);
		void setMatriz(int *matriz);

		virtual char * AplicaFiltro(char *pixels, int largura, int altura, int *maximo);
};

#endif
