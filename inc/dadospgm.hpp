#ifndef DADOSPGM_HPP
#define DADOSPGM_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

class DadosPGM{
	private:
		string P5, comentario;
		int largura, altura, maximo;
		char *pixels;

	public:
		DadosPGM();
		DadosPGM(string P5, string comentario, int largura, int altura, int maximo, char *pixels);

		string getP5();
		string getComentario();
		int getLargura();
		int getAltura();
		int getMaximo();
		char * getPixels();

		void setP5(string P5);
		void setComentario(string comentario);
		void setLargura(int largura);
		void setAltura(int altura);
		void setMaximo(int maximo);
		void setPixels(char *pixels);

		void deletePixels();
};

#endif
