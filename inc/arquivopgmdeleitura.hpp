#ifndef ARQUIVOPGMDELEITURA_HPP
#define ARQUIVOPGMDELEITURA_HPP

#include "dadospgm.hpp"

using namespace std;

class ArquivoPGMdeLeitura : public DadosPGM{
	private:
		ifstream *arquivoDeEntrada;

	public:
		ArquivoPGMdeLeitura();
		ArquivoPGMdeLeitura(ifstream *arquivoDeEntrada);

		ifstream * getArquivoDeEntrada();

		void setArquivoDeEntrada(ifstream *arquivoDeEntrada);

		int LerArquivo(const char *nome);
};

#endif
