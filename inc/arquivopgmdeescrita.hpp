#ifndef ARQUIVOPGMDEESCRITA_HPP
#define ARQUIVOPGMDEESCRITA_HPP

#include "dadospgm.hpp"

using namespace std;

class ArquivoPGMdeEscrita : public DadosPGM{
	private:
		ofstream *arquivoDeSaida;

	public:
		ArquivoPGMdeEscrita();
		ArquivoPGMdeEscrita(ofstream *arquivoDeSaida);

		ofstream * getArquivoDeSaida();

		void setArquivoDeSaida(ofstream *arquivoDeSaida);

		int EscreverArquivo(const char *nome);
};

#endif
