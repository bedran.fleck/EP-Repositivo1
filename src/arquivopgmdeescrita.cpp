#include "arquivopgmdeescrita.hpp"

using namespace std;

ArquivoPGMdeEscrita::ArquivoPGMdeEscrita(){
	setArquivoDeSaida(NULL);
}

ArquivoPGMdeEscrita::ArquivoPGMdeEscrita(ofstream *arquivoDeSaida){
	setArquivoDeSaida(arquivoDeSaida);
}

ofstream * ArquivoPGMdeEscrita::getArquivoDeSaida(){
	return arquivoDeSaida;
}

void ArquivoPGMdeEscrita::setArquivoDeSaida(ofstream *arquivoDeSaida){
	this->arquivoDeSaida = arquivoDeSaida;
}

int ArquivoPGMdeEscrita::EscreverArquivo(const char * nome){
	arquivoDeSaida->open(nome);

	if(!(arquivoDeSaida->is_open()))
		return -1;

	char *pixels = getPixels();
	int largura = getLargura();
	int altura = getAltura();

	*arquivoDeSaida << getP5() << endl;
	if(getComentario() != "vazio")
		*arquivoDeSaida << getComentario() << endl;
	*arquivoDeSaida << largura << ' ' << altura << endl << getMaximo() << endl;

	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
				arquivoDeSaida->put(pixels[i*largura+j]);
		}
	}

	arquivoDeSaida->close();

	return 0;
}
