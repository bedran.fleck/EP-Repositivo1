#include "filtro.hpp"

using namespace std;

Filtro::Filtro(){
	setDiv(1);
	setSize(3);
}

Filtro::Filtro(int div, int size){
	setDiv(div);
	setSize(size);
}

int Filtro::getDiv(){
	return div;
}

int Filtro::getSize(){
	return size;
}

int * Filtro::getMatriz(){
	return matriz;
}

void Filtro::setDiv(int div){
	this->div = div;
}
void Filtro::setSize(int size){
	this->size = size;
}

void Filtro::setMatriz(int *matriz){
	this->matriz = matriz;
}

char * Filtro::AplicaFiltro(char *pixels, int largura, int altura, int *maximo){
	int value, i, j;
	char *novos_pixels = new char[largura*altura];
	if(novos_pixels == NULL){
		return NULL;
	}

	for(i=0; i<size/2; i++){
		for(j=0; j<largura; j++){
			novos_pixels[i+j*largura] = pixels[i+j*largura];
			novos_pixels[(i+j*largura)+largura-1] = pixels[(i+j*largura)+largura-1];
		}
	}

	for(i=0; i<size/2; i++){
		for(j=0; j<altura; j++){
			novos_pixels[i*altura+j] = pixels[i*altura+j];
			novos_pixels[(j-altura*i)+(altura-1)*largura] = pixels[(j-altura*i)+(altura-1)*largura];
		}
	}

	for(i=size/2; i<largura-size/2; i++){
		for(j=size/2; j<altura-size/2; j++){
			value = 0;
			for(int x=-(size/2); x<=size/2; x++){
				for(int y=-(size/2); y<=size/2;y++){
					value += matriz[(x+1)+size*(y+1)]*(unsigned char)pixels[(i+x)+(y+j)*largura];
				}
			}
				value /= div;

				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;

				if(value > *maximo)
					*maximo = value;

				novos_pixels[i+j*largura] = value;
		}
	}

	return novos_pixels;
}
