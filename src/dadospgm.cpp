#include "dadospgm.hpp"

DadosPGM::DadosPGM(){
	setP5("P5");
	setComentario("vazio");
	setLargura(2);
	setAltura(2);
	setMaximo(255);
	setPixels(NULL);
}

DadosPGM::DadosPGM(string P5, string comentario, int largura, int altura, int maximo, char *pixels){
	setP5(P5);
	setComentario(comentario);
	setLargura(largura);
	setAltura(altura);
	setMaximo(maximo);
	setPixels(pixels);
}

string DadosPGM::getP5(){
	return P5;
}

string DadosPGM::getComentario(){
	return comentario;
}

int DadosPGM::getLargura(){
	return largura;
}

int DadosPGM::getAltura(){
	return altura;
}

int DadosPGM::getMaximo(){
	return maximo;
}

char * DadosPGM::getPixels(){
	return pixels;
}

void DadosPGM::setP5(string P5){
	this->P5 = P5;
}

void DadosPGM::setComentario(string comentario){
	this->comentario = comentario;
}

void DadosPGM::setLargura(int largura){
	this->largura = largura;
}

void DadosPGM::setAltura(int altura){
	this->altura = altura;
}

void DadosPGM::setMaximo(int maximo){
	this->maximo = maximo;
}

void DadosPGM::setPixels(char *pixels){
	this->pixels = pixels;
}

void DadosPGM::deletePixels(){
	delete pixels;
}
