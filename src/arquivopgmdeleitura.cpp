#include "arquivopgmdeleitura.hpp"

using namespace std;

ArquivoPGMdeLeitura::ArquivoPGMdeLeitura(){
	setArquivoDeEntrada(NULL);
}

ArquivoPGMdeLeitura::ArquivoPGMdeLeitura(ifstream *arquivoDeEntrada){
	setArquivoDeEntrada(arquivoDeEntrada);
}

ifstream * ArquivoPGMdeLeitura::getArquivoDeEntrada(){
	return arquivoDeEntrada;
}

void ArquivoPGMdeLeitura::setArquivoDeEntrada(ifstream *arquivoDeEntrada){
	this->arquivoDeEntrada = arquivoDeEntrada;
}

int ArquivoPGMdeLeitura::LerArquivo(const char *nome){
	arquivoDeEntrada->open(nome); //Tenta abrir o arquivo PGM
	if(!(arquivoDeEntrada->is_open()))
		return -1; //Em caso de falha, o método retorna com um código de erro

	int altura, largura, maximo;
	string P5, comentario;
	stringstream c;

	getline(*arquivoDeEntrada, P5); //Lê a primeira linha do arquivo e tenta capturar o P5
	if(P5 != "P5"){
		arquivoDeEntrada->close(); //Em caso de falha, fecha o arquivo e retorna com um código de erro
		return -1;
	}

	setP5(P5); //Grava a informação do P5
	getline(*arquivoDeEntrada, comentario);
	if(comentario[0] == '#'){
		setComentario(comentario);
		*arquivoDeEntrada >> largura >> altura;
	}
	else{
		c.str(comentario);
		c >> largura >> altura;
	}  //Verifica se há comentário e grava caso positivo
	*arquivoDeEntrada >> maximo;

	setLargura(largura);
	setAltura(altura);
	setMaximo(maximo);//Seta os atributos do arquivo de entrada

	arquivoDeEntrada->get();

	char *pixels = new char[largura*altura]; //Tenta criar a matriz de pixels.
	if(pixels == NULL){
		arquivoDeEntrada->close();
		return -1;
	}
	setPixels(pixels);//Grava a informação dos pixels na memoria.

	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
			arquivoDeEntrada->get(pixels[i*largura+j]);//Pega os valores dos pixels no arquivo.
		}
	}

	arquivoDeEntrada->close();

	return 0;
}
