#include <iostream>
#include "arquivopgmdeleitura.hpp"
#include "arquivopgmdeescrita.hpp"
#include "negativo.hpp"
#include "smooth.hpp"
#include "sharpen.hpp"

using namespace std;

int main(){

	int max;
	const char *inputName, *outputName;
	string input_name, output_name, option;
	char *mod_pixels;

	ifstream archive;
	ofstream novo;

	ArquivoPGMdeLeitura input;
	ArquivoPGMdeEscrita output;

	Negativo negative;
	Sharpen sharpen;
	Smooth smooth;

  cout << "Este programa não afeta em nada a imagem original.\nTodas as alterações são salvas em um arquivo novo no destino a ser escolhido\n";

	cout << "Digite o caminho da imagem por gentileza.\nArquivo de teste: ./doc/lena.pgm\nCaminho: ";
	cin >> input_name;

	inputName = input_name.c_str();

	input.setArquivoDeEntrada(&archive);
	output.setArquivoDeSaida(&novo);

	if(input.LerArquivo(inputName)){
		cout << "Falha ao ler arquivo PGM ou de alocação de memória. Verifique a extensão da imagem.\n";
		return 0;
	}

	while(1){
		cout << endl << "Escolha o filtro a ser aplicado\n";
		cout << "1) Copiar Imagem\n" << "2) Negativo\n" << "3) Smooth\n" << "4) Sharpen\n" << "Opção: ";
		cin >> option;
		if(option == "1" || option == "2" || option == "3" || option == "4")
			break;
		cout << "Opção inválida, tente novamente\n";
	}

	cout << endl << "Digite o caminho de destino da imagem (Exemplo: ./doc/lenaSharpen.pgm)\nCaminho: ";
	cin >> output_name;

	max =  input.getMaximo();

	if(option == "1"){
		mod_pixels = input.getPixels();
	}
	else if(option == "2"){
		mod_pixels = negative.AplicaFiltro(input.getPixels(), input.getLargura(), input.getAltura(), &max);
	}
	else if(option == "3"){
		mod_pixels = smooth.AplicaFiltro(input.getPixels(), input.getLargura(), input.getAltura(), &max);
	}
	else{
		mod_pixels = sharpen.AplicaFiltro(input.getPixels(), input.getLargura(), input.getAltura(), &max);
	}

	if(mod_pixels == NULL){
		cout << "Erro de alocação de memória!!!" << endl;
		return 0;
	}

	outputName = output_name.c_str();

	output.setP5(input.getP5());
	output.setComentario(input.getComentario());
	output.setLargura(input.getLargura());
	output.setAltura(input.getAltura());
	output.setMaximo(max);
	output.setPixels(mod_pixels);

	if(output.EscreverArquivo(outputName)){
		cout << "Falha ao gravar arquivo em disco!\nVerifique se possui as permissões necessárias para gravar arquivos neste usuário." << endl;
		return 0;
	}

	input.deletePixels();
	if(option != "1")
		output.deletePixels();

	cout << endl << "Imagem Salva na pasta de destino!" << endl;

	return 0;
}
