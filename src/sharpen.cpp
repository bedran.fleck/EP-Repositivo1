#include "sharpen.hpp"

using namespace std;

Sharpen::Sharpen(){
	int *matriz = new int[9];

	for(int i=0; i<9; i++){
		matriz[i] = (-i)%2;
		if(i == 4)
			matriz[i] = 5;
	}

	setDiv(1);
	setSize(3);
	setMatriz(matriz);
}
