#include "negativo.hpp"

using namespace std;

Negativo::Negativo(){
	return;
}

char * Negativo::AplicaFiltro(char *pixels, int largura, int altura, int *maximo){
	char *novos_pixels = new char[largura*altura];
	if(novos_pixels == NULL){
		return NULL;
	}

	for(int i=0; i<largura; i++){
		for(int j=0; j<altura; j++){
			novos_pixels[i+j*largura] = (char)(*maximo - pixels[i+j*largura]);
		}
	}

	return novos_pixels;
}
