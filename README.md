Este é um projeto voltado para fins avaliativos quanto à disciplina de Orientação a Objetos.
O objetivo deste programa é aplicar filtros diferentes a uma imagem PGM pixel a pixel, utilizando dos conceitos de orientação a objetos.

1) Para compilar este código fonte, navegue até o diretório e utilize o comando
    $make

2) Para rodar o programa, utilize:
    $make run

3) Siga as instruções dadas pelo programa para aplicar um filtro a uma imagem existente no computador.

4) Para limpar o diretório dos arquivos objeto e binários, utilize:
    $make clean

5) Documentação referente a este projeto está disponível na pasta latex sob o nome EPRepo - refman.pdf
